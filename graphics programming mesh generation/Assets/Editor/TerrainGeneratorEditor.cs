﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TerrainGenerator))]
public class TerrainGeneratorEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		TerrainGenerator script = (TerrainGenerator)target;
		if (GUILayout.Button("Generate terrain"))
		{
			script.GenerateTerrain();
		}
		if (GUILayout.Button("Clear mesh"))
		{
			script.ClearMesh();
		}
	}
}