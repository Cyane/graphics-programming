﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LODScript : MonoBehaviour
{
	[SerializeField] private Camera _camera;
	[SerializeField] private float _maxDistance, _minDistance;
	[SerializeField] private List<GameObject> _LODObjects = new List<GameObject>();
	private float _checkInterval = 0.33f, _intervalTimer = 0.0f;
	// Start is called before the first frame update
	void Start()
	{
		_camera = Camera.main;
	}

	// Update is called once per frame
	void Update()
	{
		UpdateLODCheckInterval();
	}

	private void UpdateLODCheckInterval()//interval so we don't have to do distance checks every single frame
	{
		_intervalTimer -= Time.deltaTime;
		if(_intervalTimer <= 0.0f && _camera != null)
		{
			SetLOD(CheckLODDist());
			// += random value, so distance checks of different objects don't all happen in the same frame
			_intervalTimer += _checkInterval + Random.Range(0.0f, 0.1f);
		}
	}

	private int CheckLODDist()
	{
		float dist = Vector3.Distance(_camera.transform.position, gameObject.transform.position);
		float LODDistance = (_maxDistance - _minDistance) / _LODObjects.Count;

		int LOD = (int)Mathf.Clamp(((dist - _minDistance) / LODDistance), 1f, _LODObjects.Count);

		return LOD;
	}

	private void SetLOD(int lod)
	{
		lod--;
		for (int i = 0; i < _LODObjects.Count; i++)
		{
			if (i == lod)
			{
				_LODObjects[i].SetActive(true);
			}
			else
			{
				_LODObjects[i].SetActive(false);
			}
		}
	}
}
