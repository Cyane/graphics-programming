﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class SnowParticles : MonoBehaviour
{
	private const int ARRAY_CAP = 1023;
	public bool updateSnow = true;
	public bool threading = true;
	//[SerializeField] private GameObject particlePrefab;
	[SerializeField] private Transform playerObject;
	[Header("Particle values")]
	[SerializeField] private float particleScale;
	[SerializeField] private int numberOfParticles;
	[SerializeField] private float snowAreaBoxSize;
	[SerializeField] private float gravityModifier;
	[SerializeField] private float turbulenceStrength;
	[SerializeField] private float turbulenceScale;
	[SerializeField] private Material mat;
	[SerializeField] private Mesh mesh;

	private GameObject[] _particleObjectArray;
	private Matrix4x4[] _particleTransforms;
	private List<Matrix4x4[]> matrixArrayList = new List<Matrix4x4[]>();//each array has 1023 matrices
	private SnowParticle[] _snowParticleArray;
	//private MeshFilter _mf;

	private Vector3 _gravity;
	private float _range;
	private Vector3 _particleScale;
	private float _outOfBoundsOffset = 1.0f;
	private Thread[] _threads;
	private Vector3 playerPos;
	private Quaternion playerRot;
	private float deltaTime;

	// Start is called before the first frame update

	public struct SnowParticle
	{
		public Vector3 position;
		//more data?
	}

	void Start()
	{
		_threads = new Thread[SystemInfo.processorCount];
		_range = snowAreaBoxSize * 0.5f;
		_particleScale = new Vector3(particleScale, particleScale, particleScale);
		_gravity = new Vector3(0f, -gravityModifier, 0f);
		_particleTransforms = new Matrix4x4[numberOfParticles];
		//_mf = particlePrefab.GetComponent<MeshFilter>();
		//InitParticleObjectArray();//shitty performance

		InitParticleArrays();//fills the array with SnowParticle structs and scatters the positions
							 //	initMatrixArrayList();
		
		
	}

	void Update()
	{
		//UpdateParticleTransform();

		playerPos = playerObject.transform.position;
		playerRot = playerObject.transform.rotation;
		deltaTime = Time.deltaTime;

		if (threading && updateSnow)
		{
			MultiThreadedUpdate();
		}
		else if (updateSnow)
		{
			SnowParticleMovement();//move the particles
			UpdateParticleMatrixArrayList();//Update the matrices with the new positions
		}

		if (threading)
		{
			ThreadedDrawParticleInstances();
		}
		else
		{
			DrawParticleInstances();//Draw the particles on screen this frame
		}
	}


	void MultiThreadedUpdate()
	{
		int numberOfFlakesPerThread = Mathf.FloorToInt((float)_snowParticleArray.Length / _threads.Length);
		for (int i = 0; i < _threads.Length; i++)
		{
			int firstFlake = i * numberOfFlakesPerThread;
			int lastFlake;
			if (i == _threads.Length - 1)
				lastFlake = _snowParticleArray.Length - 1;
			else
				lastFlake = (i + 1) * numberOfFlakesPerThread - 1;

			_threads[i] = new Thread(() => ThreadedSnowParticleUpdate(firstFlake,lastFlake));
			_threads[i].Start();
		}

		//foreach (Thread t in _threads)
			//t.Join();
	}

	/*
	 *Thread[] rayThreads = new Thread[Environment.ProcessorCount * 2];
		
		update(){}
			List<Snowflakes> s;
			int numberOfFlakesPerThread = s.count / threads.count;
            for (int y = 0; y < rayThreads.Length; y++)
				int firstFlake; lastFlake;
				rayThreads[y] = new Thread(() => updateSnowFlakeChunk(firstFlake,lastFlake ));
				rayThreads[y].start
			}
			foreach (Thread rayThread in rayThreads)
                rayThread.Join();
        }
		public void updateSnowFlakeChunk(int first, int last){
		}
	 
	*/

	/*
	void InitParticleObjectArray()//old method
	{
		_particleObjectArray = new GameObject[numberOfParticles];

		Vector3 minRange = new Vector3(playerObject.transform.position.x - _range, playerObject.transform.position.y - _range, playerObject.transform.position.z - _range),
			maxRange = new Vector3(playerObject.transform.position.x + _range, playerObject.transform.position.y + _range, playerObject.transform.position.z + _range);

		for (int i = 0; i < _particleObjectArray.Length; i++)
		{
			_particleObjectArray[i] = Instantiate(particlePrefab, transform);
			_particleObjectArray[i].transform.position = new Vector3(Random.Range(minRange.x, maxRange.x), Random.Range(minRange.y, maxRange.y), Random.Range(minRange.z, maxRange.z));
		}
	}

	void UpdateParticleTransform()//old method
	{
		Quaternion playerRot = playerObject.transform.rotation;
		Vector3 pos;
		for (int i = 0; i < _particleObjectArray.Length; i++)
		{
			pos = _particleObjectArray[i].transform.position;
			//combine all variables so we only have to change the transform once
			_particleObjectArray[i].transform.SetPositionAndRotation(pos += ((_gravity + Turbulence(pos)) * Time.deltaTime) + OutOfBoundsCorrection(pos), playerRot);
		}
	}
	*/


	void InitParticleArrays()
	{
		_snowParticleArray = new SnowParticle[numberOfParticles];

		Vector3 minRange = new Vector3(playerObject.transform.position.x - _range, playerObject.transform.position.y - _range, playerObject.transform.position.z - _range),
			maxRange = new Vector3(playerObject.transform.position.x + _range, playerObject.transform.position.y + _range, playerObject.transform.position.z + _range);

		for (int i = 0; i < _snowParticleArray.Length; i++)
		{
			_snowParticleArray[i] = new SnowParticle();
			_snowParticleArray[i].position = new Vector3(Random.Range(minRange.x, maxRange.x), Random.Range(minRange.y, maxRange.y), Random.Range(minRange.z, maxRange.z));
		}
	}

	void InitThreads()
	{

		//foreach (Thread t in _threads)
			//t = new Thread();
	}


	void initMatrixArrayList()
	{
		int amountOfLists = (int)Mathf.Ceil(numberOfParticles / ARRAY_CAP);
		for (int i = 0; i < amountOfLists; i++)
		{
			if (i == amountOfLists - 1)
				matrixArrayList.Add(new Matrix4x4[numberOfParticles % ARRAY_CAP]);//fill last array with the leftover amount
			else
				matrixArrayList.Add(new Matrix4x4[ARRAY_CAP]);
		}
		_particleTransforms = new Matrix4x4[numberOfParticles];
		UpdateParticleMatrixArrayList();
	}

	void SnowParticleMovement()
	{
		for (int i = 0; i < _snowParticleArray.Length; i++)
		{
			_snowParticleArray[i].position += ((_gravity + Turbulence(_snowParticleArray[i].position)) * Time.deltaTime)
				+ OutOfBoundsCorrection(_snowParticleArray[i].position);
		}
	}

	void UpdateParticleMatrixArrayList()
	{
		matrixArrayList.Clear();
		for (int i = 0, list = 0; i < _snowParticleArray.Length; list++)
		{
			Matrix4x4[] matrixArray;
			if (i + ARRAY_CAP >= _snowParticleArray.Length)
			{
				matrixArray = new Matrix4x4[_snowParticleArray.Length % ARRAY_CAP];
			}
			else
			{
				matrixArray = new Matrix4x4[ARRAY_CAP];
			}

			for (int j = 0; j < matrixArray.Length; j++, i++)
			{
				matrixArray[j].SetTRS(_snowParticleArray[i].position, playerObject.rotation, _particleScale);
			}

			matrixArrayList.Add(matrixArray);


			/*
			Matrix4x4 matrix = new Matrix4x4();
			matrix.SetTRS(_snowParticleArray[i].position, playerObject.rotation, _particleScale);
			_particleTransforms[i] = matrix;
			*/
			//matrixArrayList[Mathf.FloorToInt(i / ARRAY_CAP)][i % ARRAY_CAP] = matrix;//fuck, does this calc properly?//no it does not
		}
	}

	void ThreadedSnowParticleUpdate(int start, int finish)
	{
		for (int i = start; i < finish; i++)
		{
			//update the particle position
			_snowParticleArray[i].position += ((_gravity + Turbulence(_snowParticleArray[i].position)) * deltaTime)
				+ OutOfBoundsCorrection(_snowParticleArray[i].position);

			//update the transform matrix
			_particleTransforms[i].SetTRS(_snowParticleArray[i].position, playerRot, _particleScale);
		}
	}
	
	void DrawParticleInstances()
	{
		MaterialPropertyBlock block = new MaterialPropertyBlock();

		foreach (Matrix4x4[] matrixArray in matrixArrayList)
		{
			Graphics.DrawMeshInstanced(mesh, 0, mat, matrixArray, matrixArray.Length, block, UnityEngine.Rendering.ShadowCastingMode.Off);
		}

		/*
		for (int i = 0; i < _snowParticleArray.Length; i++)
		{
			Graphics.DrawMesh(mesh, _snowParticleArray[i].position, playerObject.transform.rotation, mat, 0);
			//Graphics.DrawMeshInstanced(mesh, 0, mat, _particleTransforms);//this has a 1023 object limit per list ffs
		}
		*/
	}

	void ThreadedDrawParticleInstances()
	{
		MaterialPropertyBlock block = new MaterialPropertyBlock();

		int batches = Mathf.CeilToInt(_particleTransforms.Length / ARRAY_CAP);
		for (int i = 0; i < batches; i++)
		{
			int batchSize = Mathf.Min(ARRAY_CAP, _particleTransforms.Length - (ARRAY_CAP * i));
			int start = i * ARRAY_CAP;

			Matrix4x4[] batchedMatrices = GetBatchedMatrices(start, batchSize);
			Graphics.DrawMeshInstanced(mesh, 0, mat, batchedMatrices, batchedMatrices.Length, block, UnityEngine.Rendering.ShadowCastingMode.Off);
		}
	}

	Vector3 Turbulence(Vector3 pos)//this function is really light on performance
	{
		Vector3 turbulence = new Vector3();

		turbulence.x = Mathf.PerlinNoise(pos.y * turbulenceScale, pos.z * turbulenceScale) * turbulenceStrength;
		turbulence.y = Mathf.PerlinNoise(pos.x * turbulenceScale, pos.z * turbulenceScale) * turbulenceStrength;
		turbulence.z = Mathf.PerlinNoise(pos.x * turbulenceScale, pos.y * turbulenceScale) * turbulenceStrength;

		return turbulence;
	}

	Vector3 OutOfBoundsCorrection(Vector3 particlePos)//this function is really light on performance
	{
		//Cache playerpos instead of getting 6 times, minor optimization
		//Vector3 playerPos = playerObject.transform.position;
		Vector3 correction = new Vector3();

		if (particlePos.x < playerPos.x - _range)
			correction += new Vector3(snowAreaBoxSize - _outOfBoundsOffset, 0f, 0f);
		else if (particlePos.x > playerPos.x + _range)
			correction += new Vector3(-snowAreaBoxSize + _outOfBoundsOffset, 0f, 0f);

		if (particlePos.y < playerPos.y - _range)
			correction += new Vector3(0f, snowAreaBoxSize - _outOfBoundsOffset, 0f);
		else if (particlePos.y > playerPos.y + _range)
			correction += new Vector3(0f, -snowAreaBoxSize + _outOfBoundsOffset, 0f);

		if (particlePos.z < playerPos.z - _range)
			correction += new Vector3(0f, 0f, snowAreaBoxSize - _outOfBoundsOffset);
		else if (particlePos.z > playerPos.z + _range)
			correction += new Vector3(0f, 0f, -snowAreaBoxSize + _outOfBoundsOffset);

		return correction;
	}

	private Matrix4x4[] GetBatchedMatrices(int offset, int batchSize)
	{
		Matrix4x4[] batchedMatrices = new Matrix4x4[batchSize];

		for (int i = 0; i < batchSize; ++i)
		{
			batchedMatrices[i] = _particleTransforms[i + offset];
		}

		return batchedMatrices;
	}
}
