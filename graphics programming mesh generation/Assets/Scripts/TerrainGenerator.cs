﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TerrainGenerator : MonoBehaviour
{
	public int cubeGridSize;
	[Header("Terrain settings")]
	public float terrainSize;
	[Range(50, 255)] public int detailDensity;
	public float power = 30.0f;
	public float scale = 0.15f;
	public int layers;
	public Material mat;

	private Mesh _mesh;
	private List<Vector2> coords;



	void Start()
	{
		Renderer renderer = gameObject.GetComponent<Renderer>();
		renderer.material = mat;
		_mesh = new Mesh();
		GenerateTerrain();

		//MakeSomeNoise();//cude generation 
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{

			//coordinates = new Vector2(Random.Range(0.0f, 1000.0f), Random.Range(0.0f, 1000.0f));
			//MakeSomeNoise();
		}
	}

	public void GenerateTerrain()
	{
		InitLayers();//randomize perlin coordinates for a new map

		int detailDensityPlus = detailDensity + 1;
		float vertexOffset = terrainSize / detailDensity;
		float parentOffset = -terrainSize / 2;

		_mesh = GetComponent<MeshFilter>().sharedMesh;
		_mesh.name = "Procedural Grid";
		_mesh.Clear();
		//CREATE VERTICES


		Vector3[] vertices = new Vector3[detailDensityPlus * detailDensityPlus];//+1 used for the vertex placement

		for (int y = 0; y < detailDensityPlus; y++)//vertex placement
		{
			for (int x = 0; x < detailDensityPlus; x++)
			{
				vertices[y * detailDensityPlus + x] = new Vector3(x * vertexOffset + parentOffset, PerlinValue(x * vertexOffset, y * vertexOffset), y * vertexOffset + parentOffset);
			}
		}
		_mesh.vertices = vertices;


		//CREATE TRIANGLES
		int[] triangles = new int[detailDensity * detailDensity * 6];//every tri calls 3 points, a square has 2 tris so * 6

		for (int y = 0, count = 0; y < detailDensity; y++)//TRIANGLES BABY
		{
			for (int x = 0; x < detailDensity; x++, count += 6)
			{
				triangles[count] = triangles[count + 3] = (y + 1) * detailDensityPlus + x;
				triangles[count + 1] = triangles[count + 5] = y * detailDensityPlus + x + 1;//use the same point
				triangles[count + 2] = y * detailDensityPlus + x;
				triangles[count + 4] = (y + 1) * detailDensityPlus + x + 1;

			}
		}
		_mesh.triangles = triangles;

		_mesh.RecalculateNormals();

		//CREATE UV MAPPING

		Vector2[] uvs = new Vector2[vertices.Length];

		for (int y = 0; y < detailDensityPlus; y++)//UV mapping
		{
			for (int x = 0; x < detailDensityPlus; x++)
			{
				uvs[x + (detailDensityPlus * y)] = new Vector2((float)x / (detailDensity), (float)y / (detailDensity));
			}
		}
		_mesh.uv = uvs;

		SetMaterial();
	}

	void InitLayers()//set perlin coordinates. Call if amount of layers changes
	{
		coords = new List<Vector2>();
		for (int i = 0; i < layers; i++)
		{
			coords.Add(new Vector2(Random.Range(0f, 1000f), Random.Range(0f, 1000f)));
		}
	}

	float PerlinValue(float x, float y)
	{
		float perlinValue = 0;
		//scale needs to be really tiny to make sense, so /100 here so that editor values can be natural numbers
		float scaledScale = scale / 100f;
		for (int k = 0; k < coords.Count; k++)
		{
			//more layers will add smaller details and need perlin coordinates that are closer together
			float layerScaler = k * scaledScale;
			perlinValue += Mathf.PerlinNoise(coords[k].x + x * (scaledScale + layerScaler), coords[k].y + y * (scaledScale + layerScaler)) / (k + 1f);
		}
		//substracting the flat number of half the amount of layers keeps the Y pos in the same place
		perlinValue -= coords.Count / (coords.Count + Mathf.Sqrt(coords.Count + 1));
		perlinValue *= power;

		return perlinValue;
	}

	void SetMaterial()
	{
		Renderer renderer = gameObject.GetComponent<Renderer>();
		renderer.material = mat;
	}

	void MakeSomeNoise()
	{
		/*
        MeshFilter mf = GetComponent<MeshFilter>();
        Vector3[] vertices = mf.mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            float xCoord = coordinates.x + vertices[i].x * scale;
            float yCoord = coordinates.y + vertices[i].z * scale;
            vertices[i].y = (Mathf.PerlinNoise(xCoord, yCoord) - 0.5f) * power;
        }
        mf.mesh.vertices = vertices;
        mf.mesh.RecalculateBounds();
        mf.mesh.RecalculateNormals();
        */

		GameObject _cubes = new GameObject("Cubes");
		float perlinValue = 0;

		InitLayers();

		for (int i = 0; i < cubeGridSize; i++)
		{
			for (int j = 0; j < cubeGridSize; j++)
			{
				for (int k = 0; k < coords.Count; k++)
				{
					perlinValue += Mathf.PerlinNoise(coords[k].x + i * scale, coords[k].y + j * (scale * ((k / 4) + 1)));
				}
				perlinValue /= coords.Count;

				GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cube.transform.position = new Vector3(i, 0f, j);
				cube.transform.localScale = new Vector3(cube.transform.localScale.x, perlinValue * power - 1f, cube.transform.localScale.z);
				cube.transform.parent = _cubes.transform;
				perlinValue = 0;
			}
		}
		_cubes.transform.localScale = new Vector3(1f, 1f, 1f);
	}
	public void ClearMesh()
	{
		_mesh.Clear();
	}


}