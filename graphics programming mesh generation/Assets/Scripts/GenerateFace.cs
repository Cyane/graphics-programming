﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GenerateFace : MonoBehaviour
{
	Mesh mesh;
	public Material mat, wireframeMat;
	public int gridXSize, gridYSize;

	public Slider xSlider, ySlider;
	public Slider ampSlider, speedSlider, lengthSlider;
	public Toggle waveToggle, wireframeToggle;
	public bool verticalPlane = true, previousState = true;
	public float wavelength, waveAmplitude, waveSpeed;
	private int oldX, oldY;
	private Vector3[] vertices;
	private Renderer renderer;

	void Start()
	{
		mesh = gameObject.GetComponent<MeshFilter>().mesh;
		Generate();
		renderer = gameObject.GetComponent<Renderer>();
		renderer.material = mat;
	}

	private void Update()
	{
		gridXSize = (int)ySlider.value;
		gridYSize = (int)xSlider.value;
		if (oldX != gridXSize || oldY != gridYSize || verticalPlane != previousState)
		{
			Generate();
		}
		if (waveToggle.isOn)
			Wave();

		if (wireframeToggle.isOn)
		{
			if (renderer.material != wireframeMat)
				renderer.material = wireframeMat;

		}
		else
		{
			if (renderer.material != mat)
				renderer.material = mat;
		}

		previousState = verticalPlane;
		oldX = gridXSize;
		oldY = gridYSize;
	}

	private void Generate()
	{
		mesh.name = "Procedural Grid";

		int gridX = gridXSize + 1;//+1 used for the vertex placement
		int gridY = gridYSize + 1;

		vertices = new Vector3[gridX * gridY];
		int[] triangles = new int[gridXSize * gridYSize * 6];//every tri calls 3 points, a square has 2 tris so * 6
		Vector2[] uvs = new Vector2[vertices.Length];

		mesh.Clear();

		for (int y = 0; y < gridY; y++)//vertex placement
		{
			for (int x = 0; x < gridX; x++)
			{
				if (verticalPlane)
					vertices[y * gridX + x] = new Vector3(x, y);
				else
					vertices[y * gridX + x] = new Vector3(x, 0f, y);
			}
		}

		mesh.vertices = vertices;

		CreateTriangles();

		mesh.RecalculateNormals();

		for (int y = 0; y < gridY; y++)//UV mapping
		{
			for (int x = 0; x < gridX; x++)
			{
				uvs[x + (gridX * y)] = new Vector2((float)x / (gridXSize), (float)y / (gridYSize));
			}
		}
		mesh.uv = uvs;
	}


	private void Wave()
	{
		//ＡＥＳＴＥＴＨＩＣＳ
		for (int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = new Vector3(vertices[i].x, ampSlider.value 
				* Mathf.Sin(lengthSlider.value * (vertices[i].x) + speedSlider.value * Time.time), vertices[i].z);
		}

		mesh.vertices = vertices;
		CreateTriangles();
		
		mesh.RecalculateNormals();
	}

	private void CreateTriangles()
	{ 
		int gridX = gridXSize + 1;
		int gridY = gridYSize + 1;

		int[] triangles = new int[gridXSize * gridYSize * 6];

		for (int y = 0, count = 0; y < gridYSize; y++)//TRIANGLES BABY
		{
			for (int x = 0; x < gridXSize; x++, count += 6)
			{
				triangles[count] = triangles[count + 3] = (y + 1) * gridX + x;
				triangles[count + 1] = triangles[count + 5] = y * gridX + x + 1;//use the same point
				triangles[count + 2] = y * gridX + x;
				triangles[count + 4] = (y + 1) * gridX + x + 1;

			}
		}
		mesh.triangles = triangles;
	}
/*
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		mesh = GetComponent<MeshFilter>().sharedMesh;

		for (int i = 0; i < mesh.vertices.Length; i++)
		{
			Gizmos.DrawSphere(mesh.vertices[i], 0.1f);
			Handles.Label(mesh.vertices[i], i.ToString());
		}
	}
	*/
}
