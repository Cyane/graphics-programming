﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateExampleLesson : MonoBehaviour
{

	Mesh mesh;
	Vector3[] vertices;
	int[] tris;//every tri calls 3 points, a square has 2 tris
	Vector2[] uvs;

	public int gridXSize, gridYSize;


	// Start is called before the first frame update
	void Start()
	{
		Generate();
	}

	void Generate()
	{
		mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();


		int gridX = gridXSize + 1;//+1 used for the vertex placement
		int gridY = gridYSize + 1;

		vertices = new Vector3[gridX * gridY];

		mesh.Clear();


		for (int x = 0; x < gridX; x++)//vertex placement
		{
			for (int y = 0; y < gridY; y++)
			{

				vertices[x * gridY + y] = new Vector3(x, y, 0f);
			}
		}

		mesh.vertices = vertices;

		tris = new int[vertices.Length * 6];

		for (int i = 0, count = 0; i < gridX; i++)
		{
			for (int j = 0; j < gridY; j++, count += 6)
			{
				tris[count] = i * gridY + j;
				tris[count + 1] = i * gridY + j + 1;
				tris[count + 2] = i * (gridY + 1) + j;
			}
		}
		mesh.triangles = tris;
	}

	// Update is called once per frame
	void Update()
	{

	}



	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		mesh = GetComponent<MeshFilter>().sharedMesh;

		for (int i = 0; i < mesh.vertices.Length; i++)
		{
			Gizmos.DrawSphere(mesh.vertices[i], 0.1f);
		}
	}
}
