﻿Shader "Hidden/ColorCorrection"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
	/*
		_Width("TextureSize", Float) = 128
		_Height("TextureSize", Float) = 128
	*/
		_Correction("correctioncolor", Color) = (1.0, 1.0, 1.0, 1.0)

		_CorrectionStrength("Correction Strength", Range(0, 1)) = 0
	}
		
	SubShader
	{

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			float _CorrectionStrength;
			uniform float4 _Correction;

			float4 frag(v2f_img i) : COLOR
			{
				float4 screen = tex2D(_MainTex, i.uv);

				float4 result = screen;
				result.rgb = lerp(screen.rgb, _Correction, _CorrectionStrength);
				return result;
			}
			ENDCG
		}
	}

}