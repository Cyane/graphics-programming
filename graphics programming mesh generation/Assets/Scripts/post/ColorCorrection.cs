﻿// https://www.alanzucconi.com/2015/07/08/screen-shaders-and-postprocessing-effects-in-unity3d/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class ColorCorrection : MonoBehaviour {

	public Color color;
    private Material material;
	[Range(0f,1f)] public float correctionStrength;

	void Awake()
	{
		material = new Material(Shader.Find("Hidden/ColorCorrection"));
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if(correctionStrength == 0f)
		{
			Graphics.Blit(source, destination);
			return;
		}
		material.SetFloat("_CorrectionStrength", correctionStrength);
		material.SetColor("_Correction", color);
		Graphics.Blit(source, destination, material);
		/*
        material.SetFloat("_Width", source.width);
		material.SetFloat("_Height", source.height);
		*/
		//apply our material to the ouput
    }
}
