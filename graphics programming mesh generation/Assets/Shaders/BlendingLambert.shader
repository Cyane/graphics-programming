﻿//HvA - GP - 2018
//https://en.wikipedia.org/wiki/Lambert%27s_cosine_law

Shader "Ruban/BlendingLambert"
{
	Properties
	{
		_DiffuseColor ("Diffuse Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_LambertScale ("Lambert Warp", Float) = 0.5
		_MaskTex("Mask", 2D) = "white" {}
		_FirstTex("Tex 1", 2D) = "white" {}
		_SecondTex("Tex 2", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase"}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"


			uniform float4 _LightColor0; 
			uniform	float4 _DiffuseColor;
			uniform float _LambertScale;

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 worldNormal : TEXCOORD3;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
			};

			sampler2D _MaskTex;
			sampler2D _FirstTex;
			sampler2D _SecondTex;


			float4 _MaskTex_ST;
			float4 _FirstTex_ST;
			float4 _SecondTex_ST;

			//verter shader
			v2f vert (appdata v)
			{
				v2f o;

				//transform vertices based on object transform (unity equivalent mul(UNITY_MATRIX_MVP, float4(pos, 1.0)))
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldNormal = normalize(mul(v.normal, (float3x3)unity_WorldToObject)); 
				o.uv = TRANSFORM_TEX(v.uv, _MaskTex);
				o.uv2 = TRANSFORM_TEX(v.uv, _FirstTex);
				o.uv3 = TRANSFORM_TEX(v.uv, _SecondTex);

				return o;
			}
			
			//pixel shader
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 mask = tex2D(_MaskTex, i.uv);
				fixed4 first = tex2D(_FirstTex, i.uv2);
				fixed4 second = tex2D(_SecondTex, i.uv3);


				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);//Pos0 gives the direction vector of the first directional light in the scene

				float NdotL = dot(i.worldNormal, lightDir);
				NdotL = NdotL * _LambertScale + _LambertScale;
				float3 diffuse = _DiffuseColor * _LightColor0.rgb * NdotL;

				float4 dif = (diffuse,0.5);

				float4 blend = lerp(first, second, mask.r);

				float4 lambertBlend;
				lambertBlend.rgb = (blend.rgb + diffuse) / 2;
				return lambertBlend;
			}

			ENDCG
		}
	}
		Fallback "Diffuse"
}
